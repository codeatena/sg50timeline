//
//  KMAppDelegate.m
//  Example
//
//  Created by Mikael Konutgan on 03/05/2013.
//  Copyright (c) 2013 Mikael Konutgan. All rights reserved.
//

#import "KMAppDelegate.h"
#import "KMMasterViewController.h"
#import "KMWebViewController.h"
#import "PBWebViewController.h"
#import "PBSafariActivity.h"
#import <Parse/Parse.h>

PBWebViewController *master;

@implementation KMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024
                                                         diskCapacity:20 * 1024 * 1024
                                                             diskPath:nil];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [NSURLCache setSharedURLCache:URLCache];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window.backgroundColor = [UIColor whiteColor];
    
    KMMasterViewController *masterViewController = [[KMMasterViewController alloc] init];
    PBSafariActivity *activity = [[PBSafariActivity alloc] init];
    master = [[PBWebViewController alloc]init];
    master.URL = [NSURL URLWithString:@"http://sg50timeline.com/"];
    master.applicationActivities = @[activity];
    master.excludedActivityTypes = @[UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToWeibo];
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:master];
        self.window.rootViewController = self.navigationController;
//    } else {
//        UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
//        KMWebViewController *webViewController = [[KMWebViewController alloc] init];
//        UINavigationController *webNavigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
//    	masterViewController.webViewController = webViewController;
//        self.splitViewController = [[UISplitViewController alloc] init];
//        self.splitViewController.delegate = webViewController;
//        self.splitViewController.viewControllers = @[masterNavigationController, webNavigationController];
//        self.window.rootViewController = self.splitViewController;
//    }
    [self.window makeKeyAndVisible];
    self.navigationController.navigationBarHidden = YES;
    
    [Parse setApplicationId:@"QUlyYv50OtrMxB5L2Oddn3QpC6vMu5NdULM4idev" clientKey:@"M4TMl5RtmdQ3Oq0IGZv8WCz3mC4pKe3VvMx21DBN"];
    // Override point for customization after application launch.
    
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    

    
    return YES;
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[ @"global" ];
    [currentInstallation saveInBackground];
    
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        [master load];
    }
}

@end
